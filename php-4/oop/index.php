<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");

echo "Nama Animal : $sheep->name <br>"; // "shaun"
echo "Jumlah Legs : $sheep->legs <br>"; // 2
echo "Berdarah Dingin : $sheep->cold_blooded <br> <br>";// false

$sungokong = new Ape("kera sakti");

echo "Nama Animal : $sungokong->name <br>"; 
echo "Jumlah Legs : $sungokong->legs <br>";
echo "Berdarah Dingin : $sungokong->cold_blooded <br>";
echo "Bunyi : "; $sungokong->yell();

echo"<br> <br>";

$kodok = new Frog("buduk");
echo "Nama Animal : $kodok->name <br>"; 
echo "Jumlah Legs : $kodok->legs <br>";
echo "Berdarah Dingin : $kodok->cold_blooded <br>";
echo "Bunyi : "; $kodok->jump() ; // "hop hop"


